#!/usr/bin/env ruby
# frozen_string_literal: true

require 'securerandom'
require_relative '../lib/tinycode'

ITERATIONS = 100_000
MAX_INPUT_LENGTH = 4096

$stdout.sync

MAX_INPUT_LENGTH.times do |input_length|
  ITERATIONS.times do
    input = SecureRandom.bytes(input_length)
    begin
      Tinycode.load(input)
    rescue Tinycode::ParseError
      # pass
    rescue Error
      warn "Attempted to parse #{input.inspect} but got unexpected error:"
      raise
    end
  end

  print '.'
  puts " #{input_length + 1} of #{MAX_INPUT_LENGTH}" if input_length % 64 == 63
end

puts
puts 'Congratulations! Was unable to find a vector that resulted in an ' \
  "unexpected error for sizes <= #{MAX_INPUT_LENGTH}"
