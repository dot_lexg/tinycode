# frozen_string_literal: true

require 'bundler/setup'
require 'bundler/gem_tasks'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'yard'

RSpec::Core::RakeTask.new(:spec)
RuboCop::RakeTask.new(:lint) do |task|
  task.options = %w[--except Metrics]
end
YARD::Rake::YardocTask.new

task :dev do
  sh 'bundle install'
  sh 'rerun -p "{**/*.{rb,gemspec},exe/**/*,Gemfile*,Rakefile}" -x rake yard spec'
end

task :autofixlint do
  sh 'rerun -p "{**/*.{rb,gemspec},exe/**/*,Gemfile*,Rakefile}" -x rake lint:auto_correct'
end

task default: [:spec, :lint]
