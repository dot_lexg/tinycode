# Tinycode

This gem provides an encoder and decoder for tinycode, which is a [Bencode] like
encoding that supports more data-types and slightly smaller encoded sizes.

The encoder supports arrays, hashes, strings, symbols, and integers, as well as
specialized fixed-width integers: `Tinycode::U8`, `Tinycode::U16`,
`Tinycode::U32`, and `Tinycode::U64`.
Like [YAML] but unlike [JSON], this encoder maintains a distinction between
strings and symbols.

The encoding is inspired by Bencode's tag-length-value construction for strings, which eliminates the need for escaping in Strings.
This makes this encoding particularly appropriate for serializing binary strings
directly.

[YAML]: https://ruby-doc.org/stdlib-3.0.2/libdoc/yaml/rdoc/YAML.html
[JSON]: https://ruby-doc.org/stdlib-3.0.2/libdoc/json/rdoc/JSON.html
[Bencode]: https://en.wikipedia.org/wiki/Bencode

## Demo

Consider the following data structure:

```ruby
[
  {
  location: 'Chicago',
  currently: {
    temperature: 72,
    condition: ['Partly cloudy', 'Chance of rain']
  },
  narrative: "High: 81, low: 56\nCooler than yesterday.",
  high: 81,
  low: 56
  },
  {
  location: 'New York',
  currently: {
    temperature: 45,
    condition: ['Rainy']
  },
  narrative: "High: 56, low: -2\nWinter Weather Alert in effect.",
  high: 56,
  low: -2
  }
]
```

The resulting structure looks something like this. In actuality, there is not
this much whitespace, and the \xNN codes represent binary values.

```plain
[
  {
    currently:{
      condition:[
        Partly cloudy'
        Chance of rain'
      ]
      temperature:+\x48
    }
    high:+\x51
    location:Chicago'
    low:+\x38
    narrative:'\x28High: 81, low: 56\nCooler than yesterday.
  }
  {
    currently:{condition:[Rainy']temperature:+\x2D}
    high:+\x38
    location:New York'
    low:-\x02
    narrative:'\x31High: 56, low: -2\nWinter Weather Alert in effect.
  }
]
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tinycode'
```

And then execute:

```sh
bundle install
```

Or install it yourself as:

```sh
gem install tinycode
```

## Usage

### Dumping a data structure

```ruby
require 'tinycode'

42.tinycode
['some', 'array'].tinycode
# and so on
```

Alternatively you can use `Tinycode.dump`:

```ruby
# can use 'tinycode/no_core_ext' instead if class extensions are unwanted
require 'tinycode'

Tinycode.dump(42)
Tinycode.dump(['some', 'array'])
# and so on
```

### Restoring a data structure

```ruby
require 'tinycode'

Tinycode.load("+\x2A".b) # => 42
Tinycode.load("[some'array']".b) # => ['some', 'array']
# and so on
```

## Development

After checking out the repo, run `bundle install` to install dependencies. Then,
run `rake watch` to run yard, tests, and linting on every file save.

To install this gem onto your local machine, run `rake install`.

To release a new version, update the version number in `version.rb`.
Then run `rake release`, which will create/push a git tag for the version and
publish the `.gem` file to [rubygems.org].

[rubygems.org]: https://rubygems.org

## License

All copyright rights are held by the authors as recorded in git.
See the commit history for a comprehensive listing of copyright holders and
copyright dates.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See LICENSE in this repository for the full license text.
