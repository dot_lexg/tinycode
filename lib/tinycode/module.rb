# frozen_string_literal: true

require_relative 'fixed_integer'
require_relative 'version'

# An encoder and decoder for tinycode, which is a bencode like encoding that
# supports more data-types and slightly smaller encoded sizes.
module Tinycode
  class ParseError < StandardError; end

  class << self
    def dump(obj)
      case obj
      when Array then dump_array(obj)
      when Hash then dump_hash(obj)
      when String then dump_string(obj)
      when Symbol then dump_symbol(obj)
      when Integer then dump_integer(obj)
      when U8 then dump_fixed_integer(obj, code: '%', pack: 'aC')
      when U16 then dump_fixed_integer(obj, code: '$', pack: 'aS>')
      when U32 then dump_fixed_integer(obj, code: '!', pack: 'aL>')
      when U64 then dump_fixed_integer(obj, code: '#', pack: 'aQ>')
      when nil then "\0".b
      when true then '?T'.b
      when false then '?F'.b
      else raise ArgumentError, "Don't know how to dump #{obj.class}"
      end
    end

    def load(tinycode)
      tinycode = implicitly_convert_str(tinycode).b
      struct, length = load_with_length(tinycode)
      raise 'a load_ implementation claims it read past the end of stream!' if length > tinycode.length
      raise ParseError, 'Stream is longer than expected' if length < tinycode.length

      struct
    end

    private

    def implicitly_convert_str(obj)
      raise ArgumentError, "Cannot convert #{obj.class} to String" if !obj.respond_to?(:to_str)

      str = obj.to_str
      raise ArgumentError, "#{obj.class}#to_str did not return String, got #{str.class}" if !str.is_a?(String)

      str
    end

    def load_with_length(tinycode)
      case tinycode[0]
      when /[\w \t\n]/ then load_id(tinycode)
      when '['         then load_array(tinycode)
      when '{'         then load_hash(tinycode)
      when '@'         then load_binary_string(tinycode)
      when "'"         then load_string(tinycode)
      when ':'         then load_symbol(tinycode)
      when '-', '+'    then load_integer(tinycode)
      when '%'         then load_fixed_integer(tinycode, U8, length: 1, unpack: 'C')
      when '$'         then load_fixed_integer(tinycode, U16, length: 2, unpack: 'S>')
      when '!'         then load_fixed_integer(tinycode, U32, length: 4, unpack: 'L>')
      when '#'         then load_fixed_integer(tinycode, U64, length: 8, unpack: 'Q>')
      when "\0"        then [nil, 1]
      when '?'         then load_boolean(tinycode)
      when nil then raise ParseError, 'Unexpected end of stream'
      else          raise ParseError, "Unexpected character: #{tinycode[0].inspect}"
      end
    end

    def dump_array(ary)
      "[#{ary.map(&method(:dump)).join}]".b
    end

    def load_array(tinycode)
      offset = 1
      result = []
      until tinycode[offset] == ']'
        value, length = load_with_length(tinycode[offset..])
        result << value
        offset += length
      end
      [result, offset + 1]
    end

    def dump_hash(hash)
      "{#{hash.sort.flatten(1).map(&method(:dump)).join}}".b
    end

    def load_hash(tinycode)
      offset = 1
      result = {}
      until tinycode[offset] == '}'
        key, key_length = load_with_length(tinycode[offset..])
        offset += key_length
        value, value_length = load_with_length(tinycode[offset..])
        offset += value_length
        raise if result.key?(key)

        result[key] = value
      end
      [result, offset + 1]
    end

    def dump_string(str, code: "'")
      if str.encoding == Encoding::BINARY
        raise if code != "'"

        code = '@'
      else
        str = str.encode('UTF-8').b
      end

      if str =~ /\A[\w \t\n][^':]*\z/
        [str, code].pack('a*a')
      else
        [code, str.length, str].pack('awa*')
      end
    end

    def load_id(tinycode)
      length = tinycode.index(/['@:]/)
      raise ParseError, 'Unexpected end of stream' if !length

      value = tinycode[0...length]

      case tinycode[length]
      when "'"
        value.force_encoding('UTF-8')
        raise ParseError, 'invalid byte sequence in UTF-8 string' if !value.valid_encoding?
      when '@'
        # value is already binary
      when ':'
        value.force_encoding('UTF-8')
        raise ParseError, 'invalid byte sequence in UTF-8 string' if !value.valid_encoding?

        value = value.to_sym
      else raise
      end

      [value, length + 1]
    end

    def load_binary_string(tinycode)
      str_length, offset = load_varint(tinycode[1..])
      raise ParseError, 'Unexpected end of stream' if tinycode.length < 1 + offset + str_length

      str = tinycode[(1 + offset)...(1 + offset + str_length)]

      [str, 1 + offset + str_length]
    end

    def load_string(tinycode)
      value, offset = load_binary_string(tinycode)
      value.force_encoding('UTF-8')
      raise ParseError, 'invalid byte sequence in UTF-8 string' if !value.valid_encoding?

      [value, offset]
    end

    def dump_symbol(sym)
      raise ArgumentError, "Symbol #{sym.inspect} must not be binary encoded" if sym.encoding == Encoding::BINARY

      dump_string(sym.to_s, code: ':')
    end

    def load_symbol(tinycode)
      value, offset = load_string(tinycode)
      [value.to_sym, offset]
    end

    def dump_integer(num)
      if num < 0
        ['-', -num].pack('aw')
      else
        ['+', num].pack('aw')
      end
    end

    def load_integer(tinycode)
      value, offset = load_varint(tinycode[1..])
      value = -value if tinycode[0] == '-'
      raise ParseError, '-0 is not a valid integer' if value == 0 && tinycode[0] == '-'

      [value, 1 + offset]
    end

    def dump_fixed_integer(num, code:, pack:)
      [code, num].pack(pack)
    end

    def load_fixed_integer(tinycode, clazz, length:, unpack:)
      raise ParseError, 'Unexpected end of stream' if tinycode.length < length + 1

      [clazz.new(tinycode[1...(1 + length)].unpack1(unpack)), 1 + length]
    end

    def load_varint(tinycode)
      length = tinycode.each_byte.lazy.take_while {|b| b > 0x7F }.count + 1
      raise ParseError, 'Unexpected end of stream' if length > tinycode.length

      [tinycode[0...length].unpack1('w'), length]
    end

    def load_boolean(tinycode)
      case tinycode[1]
      when 'T' then [true, 2]
      when 'F' then [false, 2]
      else raise ParseError, 'Malformed boolean value'
      end
    end
  end
end
