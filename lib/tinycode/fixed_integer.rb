# frozen_string_literal: true

module Tinycode
  # Mixin module for U8, U16, U32, and U64 classes
  module FixedInteger
    attr_reader :value
    alias to_i value
    alias to_int value

    def initialize(value)
      raise ArgumentError, "#{value.class} cannot be converted to integer" unless value.respond_to?(:to_int)

      @value = value.to_int

      raise RangeError, "#{@value.inspect} out of range for #{self.class}" unless self.class.range.include?(@value)
    end

    def tinycode
      Tinycode.dump(self)
    end

    def to_s
      value.to_s
    end

    def inspect
      "#<#{self.class} value=#{value.inspect}>"
    end

    def ==(other)
      value == other.value
    end
  end

  # an unsigned integer that takes up exactly 8 bits
  class U8
    include FixedInteger
    def self.range
      0x00..0xFF
    end
  end

  # an unsigned integer that takes up exactly 16 bits
  class U16
    include FixedInteger
    def self.range
      0x0000..0xFFFF
    end
  end

  # an unsigned integer that takes up exactly 32 bits
  class U32
    include FixedInteger
    def self.range
      0x00000000..0xFFFFFFFF
    end
  end

  # an unsigned integer that takes up exactly 64 bits
  class U64
    include FixedInteger
    def self.range
      0x00000000_00000000..0xFFFFFFFF_FFFFFFFF
    end
  end
end
