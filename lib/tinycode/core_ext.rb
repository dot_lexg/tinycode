# frozen_string_literal: true

# rubocop:disable Style/Documentation


class Array
  def tinycode
    Tinycode.dump(self)
  end
end

class Hash
  def tinycode
    Tinycode.dump(self)
  end
end

class String
  def tinycode
    Tinycode.dump(self)
  end
end

class Symbol
  def tinycode
    Tinycode.dump(self)
  end
end

class Integer
  def tinycode
    Tinycode.dump(self)
  end
end

class TrueClass
  def tinycode
    Tinycode.dump(self)
  end
end

class FalseClass
  def tinycode
    Tinycode.dump(self)
  end
end

class NilClass
  def tinycode
    Tinycode.dump(self)
  end
end

# rubocop:enable Style/Documentation
