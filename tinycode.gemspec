# frozen_string_literal: true

require_relative 'lib/tinycode/version'

Gem::Specification.new do |spec|
  spec.name        = 'tinycode'
  spec.version     = Tinycode::VERSION
  spec.summary     = 'An encoder and decoder for tinycode'
  spec.description = <<~DESCRIPTION
    tinycode is a bencode like encoding that supports more data-types
    and slightly smaller encoded sizes.
  DESCRIPTION
  spec.author      = 'Alex Gittemeier'
  spec.email       = 'me@a.lexg.dev'
  spec.license     = 'GPL-3.0-only'

  spec.metadata['homepage_uri']          = 'https://gitlab.com/dot_lexg/tinycode'
  spec.metadata['source_code_uri']       = 'https://gitlab.com/dot_lexg/tinycode/-/tree/stable'
  spec.metadata['documentation_uri']     = 'https://www.rubydoc.info/gems/tinycode/'
  spec.metadata['bug_tracker_uri']       = 'https://gitlab.com/dot_lexg/tinycode/-/issues'
  spec.metadata['changelog_uri']         = 'https://gitlab.com/dot_lexg/tinycode/-/commits/stable'
  spec.metadata['rubygems_mfa_required'] = 'true'
  spec.homepage = spec.metadata['homepage_uri']

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end

  spec.required_ruby_version = '>= 2.6.0'

  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rerun', '~> 0.13'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 1.21'
  spec.add_development_dependency 'rubocop-rake', '~> 0.6'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.6'
  spec.add_development_dependency 'simplecov', '~> 0.21'
  spec.add_development_dependency 'yard', '~> 0.9'
end
