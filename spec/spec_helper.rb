# frozen_string_literal: true

require 'bundler/setup'
require 'simplecov'

SimpleCov.start

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true # default in RSpec 4
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true # default in RSpec 4
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups # default in Rspec 4

  # enable fit, fdescribe, fcontext
  config.filter_run_when_matching :focus

  # Allow rspec to persist state here. Not version tracked.
  config.example_status_persistence_file_path = 'spec/results.log'

  config.disable_monkey_patching!
  config.warnings = true

  # Use the documentation formatter for detailed output when running one file,
  # unless a formatter was specified by a command-line flag.
  config.default_formatter = 'doc' if config.files_to_run.one?

  # Print the 10 slowest examples and example groups at the
  # end of the spec run, to help surface which specs are running
  # particularly slow.
  config.profile_examples = 3

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random
  Kernel.srand config.seed
end
