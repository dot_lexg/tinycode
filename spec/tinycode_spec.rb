# frozen_string_literal: true

require 'tinycode'

class EvilToStr
  def to_str
    :non_string
  end
end

RSpec.describe Tinycode do
  let(:complex_datastructure) do
    [
      {
        location: 'Chicago',
        currently: {
          temperature: 72,
          condition: ['Partly cloudy', 'Chance of rain']
        },
        narrative: "High: 81, low: 56\nCooler than yesterday.",
        high: 81,
        low: 56
      },
      {
        location: 'New York',
        currently: {
          temperature: 45,
          condition: ['Rainy']
        },
        narrative: "High: 56, low: -2\nWinter Weather Alert in effect.",
        high: 56,
        low: -2
      }
    ]
  end

  let(:complex_datastructure_encoded) do
    '[{' \
      "currently:{condition:[Partly cloudy'Chance of rain']temperature:+\x48}" \
      "high:+\x51" \
      "location:Chicago'" \
      "low:+\x38" \
      "narrative:'\x28High: 81, low: 56\nCooler than yesterday." \
      '}{' \
      "currently:{condition:[Rainy']temperature:+\x2D}" \
      "high:+\x38" \
      "location:New York'" \
      "low:-\x02" \
      "narrative:'\x31High: 56, low: -2\nWinter Weather Alert in effect." \
      '}]'.b
  end


  describe '.dump' do
    it 'encodes Arrays' do
      expect(described_class.dump([])).to eq('[]'.b)
      expect(described_class.dump([[]])).to eq('[[]]'.b)
      expect(described_class.dump([[], []])).to eq('[[][]]'.b)
    end

    it 'encodes Hashes' do
      expect(described_class.dump({})).to eq('{}'.b)
      expect(described_class.dump(greet: 'hey', where: 'there')).to eq("{greet:hey'where:there'}".b)
      expect(described_class.dump('string_keys' => 'test')).to eq("{string_keys'test'}".b)
    end

    it 'sorts the keys in Hashes' do
      expect(described_class.dump(where: 'there', greet: 'hey')).to eq("{greet:hey'where:there'}".b)
    end

    it 'encodes Strings' do
      expect(described_class.dump('')).to eq("'\x00".b)
      expect(described_class.dump('an_identifier')).to eq("an_identifier'".b)
      expect(described_class.dump("hello world\0")).to eq("hello world\x00'".b)
      expect(described_class.dump("hello world'")).to eq("'\x0Chello world'".b)
      expect(described_class.dump("\u{1f44f} handclap")).to eq("'\x0D\xF0\x9F\x91\x8F handclap".b)
    end

    it 'encodes Strings (binary)' do
      expect(described_class.dump(''.b)).to eq("@\x00".b)
      expect(described_class.dump('an_identifier'.b)).to eq('an_identifier@'.b)
      expect(described_class.dump("hello world\0".b)).to eq("hello world\x00@".b)
      expect(described_class.dump("hello world'".b)).to eq("@\x0Chello world'".b)
    end

    it 'encodes Strings (UTF-16)' do
      expect(described_class.dump(''.encode('UTF-16'))).to eq("'\x00".b)
      expect(described_class.dump('an_identifier'.encode('UTF-16'))).to eq("an_identifier'".b)
      expect(described_class.dump("hello world\0".encode('UTF-16'))).to eq("hello world\x00'".b)
      expect(described_class.dump("hello world'".encode('UTF-16'))).to eq("'\x0Chello world'".b)
      expect(described_class.dump("\u{1f44f} handclap".encode('UTF-16'))).to eq("'\x0D\xF0\x9F\x91\x8F handclap".b)
    end

    it 'encodes Symbols' do
      expect(described_class.dump(:"")).to eq(":\x00".b)
      expect(described_class.dump(:symbol)).to eq('symbol:'.b)
    end

    it 'refuses to encode binary symbols' do
      expect { described_class.dump("\xFF".b.to_sym) }.to raise_error(ArgumentError,
        'Symbol :"\xFF" must not be binary encoded')
    end

    it 'encodes Integers' do
      expect(described_class.dump(-123_456_789)).to eq("-\xBA\xEF\x9A\x15".b)
      expect(described_class.dump(-1)).to eq("-\x01".b)
      expect(described_class.dump(0)).to eq("+\x00".b)
      expect(described_class.dump(1)).to eq("+\x01".b)
      expect(described_class.dump(123_456_789)).to eq("+\xBA\xEF\x9A\x15".b)
    end

    it 'encodes Tinycode::U8' do
      expect(described_class.dump(Tinycode::U8.new(0x00))).to eq("%\x00".b)
      expect(described_class.dump(Tinycode::U8.new(0x01))).to eq("%\x01".b)
      expect(described_class.dump(Tinycode::U8.new(0x6F))).to eq("%\x6F".b)
      expect(described_class.dump(Tinycode::U8.new(0xFF))).to eq("%\xFF".b)
    end

    it 'encodes Tinycode::U16' do
      expect(described_class.dump(Tinycode::U16.new(0x0000))).to eq("$\x00\x00".b)
      expect(described_class.dump(Tinycode::U16.new(0x0001))).to eq("$\x00\x01".b)
      expect(described_class.dump(Tinycode::U16.new(0x5EF2))).to eq("$\x5E\xF2".b)
      expect(described_class.dump(Tinycode::U16.new(0xFFFF))).to eq("$\xFF\xFF".b)
    end

    it 'encodes Tinycode::U32' do
      expect(described_class.dump(Tinycode::U32.new(0x00000000))).to eq("!\x00\x00\x00\x00".b)
      expect(described_class.dump(Tinycode::U32.new(0x00000001))).to eq("!\x00\x00\x00\x01".b)
      expect(described_class.dump(Tinycode::U32.new(0x6AC0C1C3))).to eq("!\x6A\xC0\xC1\xC3".b)
      expect(described_class.dump(Tinycode::U32.new(0xFFFFFFFF))).to eq("!\xFF\xFF\xFF\xFF".b)
    end

    it 'encodes Tinycode::U64' do
      expect(described_class.dump(Tinycode::U64.new(0x00000000_00000000))).to eq("#\x00\x00\x00\x00\x00\x00\x00\x00".b)
      expect(described_class.dump(Tinycode::U64.new(0x00000000_00000001))).to eq("#\x00\x00\x00\x00\x00\x00\x00\x01".b)
      expect(described_class.dump(Tinycode::U64.new(0x6F832225_FF0300FF))).to eq("#\x6F\x83\x22\x25\xFF\x03\x00\xFF".b)
      expect(described_class.dump(Tinycode::U64.new(0xFFFFFFFF_FFFFFFFF))).to eq("#\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF".b)
    end

    it('encodes true, false, nil') do
      expect(described_class.dump(true)).to eq('?T'.b)
      expect(described_class.dump(false)).to eq('?F'.b)
      expect(described_class.dump(nil)).to eq("\x00".b)
    end

    it 'encodes complex data structures' do
      expect(described_class.dump(complex_datastructure)).to eq(complex_datastructure_encoded)
    end

    it 'does not encode other classes' do
      expect { described_class.dump(described_class) }.to raise_error(ArgumentError, "Don't know how to dump Module")
      expect { described_class.dump(EvilToStr.new) }.to raise_error(ArgumentError, "Don't know how to dump EvilToStr")
    end

    # TODO: encode types that are implicitly convertible?
  end

  describe '.load' do
    def expect_reflexive(test)
      reflexive = Tinycode.load(Tinycode.dump(test))
      expect(reflexive).to eq(test)
      expect(reflexive.encoding).to eq(test.encoding) if test.respond_to?(:encoding)
    end

    it 'loads Arrays' do
      expect_reflexive([])
      expect_reflexive([[]])
      expect_reflexive([[], []])
    end

    it 'loads Hashes' do
      expect_reflexive({})
      expect_reflexive(greet: 'hey', where: 'there')
      expect_reflexive('string_keys' => 'test')
    end

    it 'loads Strings' do
      expect_reflexive('')
      expect_reflexive('an_identifier')
      expect_reflexive("hello world\0")
      expect_reflexive("hello world'")
      expect_reflexive("\u{1f44f} handclap")
    end

    it 'loads Strings that were unnecessarily encoded as TLV' do
      expect(described_class.load("'#{13.chr}an_identifier")).to eq('an_identifier')
      expect(described_class.load("'#{12.chr}hello world\0")).to eq("hello world\0")
    end

    it 'encodes Strings (binary)' do
      expect_reflexive(''.b)
      expect_reflexive('an_identifier'.b)
      expect_reflexive("hello world\0".b)
      expect_reflexive("hello world'".b)
      expect_reflexive("\xFF\xFF\xFF\xFF".b)
    end

    it 'loads Symbols' do
      expect_reflexive(:"")
      expect_reflexive(:symbol)
    end

    it 'loads Integers' do
      expect_reflexive(-123_456_789)
      expect_reflexive(-1)
      expect_reflexive(0)
      expect_reflexive(1)
      expect_reflexive(123_456_789)
    end

    it 'loads Tinycode::U8' do
      expect_reflexive(Tinycode::U8.new(0x00))
      expect_reflexive(Tinycode::U8.new(0x01))
      expect_reflexive(Tinycode::U8.new(0x6F))
      expect_reflexive(Tinycode::U8.new(0xFF))
    end

    it 'loads Tinycode::U16' do
      expect_reflexive(Tinycode::U16.new(0x0000))
      expect_reflexive(Tinycode::U16.new(0x0001))
      expect_reflexive(Tinycode::U16.new(0x5EF2))
      expect_reflexive(Tinycode::U16.new(0xFFFF))
    end

    it 'loads Tinycode::U32' do
      expect_reflexive(Tinycode::U32.new(0x0000_0000))
      expect_reflexive(Tinycode::U32.new(0x0000_0001))
      expect_reflexive(Tinycode::U32.new(0x6AC0_C1C3))
      expect_reflexive(Tinycode::U32.new(0xFFFF_FFFF))
    end

    it 'loads Tinycode::U64' do
      expect_reflexive(Tinycode::U64.new(0x0000_0000_0000_0000))
      expect_reflexive(Tinycode::U64.new(0x0000_0000_0000_0001))
      expect_reflexive(Tinycode::U64.new(0x6F83_2225_FF03_00FF))
      expect_reflexive(Tinycode::U64.new(0xFFFF_FFFF_FFFF_FFFF))
    end

    it('loads true, false, nil') do
      expect_reflexive(true)
      expect_reflexive(false)
      expect_reflexive(nil)
    end

    it 'loads complex data structures' do
      expect_reflexive(complex_datastructure)
    end

    it 'raises ArgumentError if passed a non-String' do
      expect { described_class.load(nil) }.to raise_error(ArgumentError, 'Cannot convert NilClass to String')
      expect { described_class.load(:symbol) }.to raise_error(ArgumentError, 'Cannot convert Symbol to String')
      expect { described_class.load(EvilToStr.new) }.to raise_error(ArgumentError,
        'EvilToStr#to_str did not return String, got Symbol')
    end

    describe 'refuses to load bad inputs' do
      example 'Unsupported opcode' do
        expect { described_class.load('=stuff') }.to raise_error(Tinycode::ParseError, 'Unexpected character: "="')
        expect { described_class.load('>stuff') }.to raise_error(Tinycode::ParseError, 'Unexpected character: ">"')
        expect { described_class.load(']stuff') }.to raise_error(Tinycode::ParseError, 'Unexpected character: "]"')
        expect { described_class.load('}stuff') }.to raise_error(Tinycode::ParseError, 'Unexpected character: "}"')
        expect { described_class.load(']') }.to raise_error(Tinycode::ParseError, 'Unexpected character: "]"')
        expect { described_class.load('}') }.to raise_error(Tinycode::ParseError, 'Unexpected character: "}"')
        expect { described_class.load("\xFF") }.to raise_error(Tinycode::ParseError, 'Unexpected character: "\xFF"')
      end

      example 'Two valid streams concatenated' do
        expect { described_class.load("one'two'") }.to raise_error(Tinycode::ParseError,
          'Stream is longer than expected')
      end

      example 'unterminated simple string/symbol' do
        expect { described_class.load('unterminated string or symbol') }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load('u[]') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
      end

      example 'Bad UTF-8 in string/symbol' do
        expect { described_class.load("some text\xFF\xFF'") }.to raise_error(Tinycode::ParseError,
          'invalid byte sequence in UTF-8 string')
        expect { described_class.load("some text\xFF\xFF:") }.to raise_error(Tinycode::ParseError,
          'invalid byte sequence in UTF-8 string')
      end

      example 'Array with no terminating ]' do
        expect { described_class.load('[') }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load("[array'with'no'terminus'") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load("[array'with'wrong'terminus'}") }.to raise_error(Tinycode::ParseError,
          'Unexpected character: "}"')
        expect { described_class.load("[[array'with'no']terminus'") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
      end

      example 'Hash with no terminating }' do
        expect { described_class.load('{') }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load("{hash:with'no:terminus'") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load("{hash:with'wrong:terminus']") }.to raise_error(Tinycode::ParseError,
          'Unexpected character: "]"')
        expect { described_class.load("{a:{hash:with'}no:terminus'") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
      end

      example 'Hash with an odd number of entries' do
        expect { described_class.load('{hash:}') }.to raise_error(Tinycode::ParseError,
          'Unexpected character: "}"')
        expect { described_class.load("{hash:with'odd:number'of_entries'}") }.to raise_error(Tinycode::ParseError,
          'Unexpected character: "}"')
      end

      example 'TLV string/symbol whose length extends past end of stream' do
        expect { described_class.load("'#{72.chr}short string") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load(":#{72.chr}short symbol") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
      end

      example 'TLV string/symbol whose length is longer than sizeof(long)' do
        expect { described_class.load("'\xA5\x82\xA6\x92\xC3\xAD\xAA\xFB\xEBM") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load(":\xA5\x82\xA6\x92\xC3\xAD\xAA\xFB\xEBM") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
      end

      example 'Bad UTF-8 in TLV string/symbol' do
        expect { described_class.load("'\x01\x8C") }.to raise_error(Tinycode::ParseError,
          'invalid byte sequence in UTF-8 string')
        expect { described_class.load(":\x01\x8C") }.to raise_error(Tinycode::ParseError,
          'invalid byte sequence in UTF-8 string')
      end

      example 'TLV string/symbol with no length at all' do
        expect { described_class.load("'") }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load(':') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
      end

      example 'Varint without any hex < 128 upon reaching end of stream' do
        expect { described_class.load("+#{129.chr}") }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
      end

      example 'Varint at end of stream' do
        expect { described_class.load('+') }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
        expect { described_class.load('-') }.to raise_error(Tinycode::ParseError,
          'Unexpected end of stream')
      end

      example 'Minus zero' do
        expect { described_class.load("-\0") }.to raise_error(Tinycode::ParseError,
          '-0 is not a valid integer')
      end

      example 'Fixed length integers of incorrect stream length' do
        expect { described_class.load('%') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load('$') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load('!') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load('#') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load('$a') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load('!abc') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
        expect { described_class.load('#abcdefg') }.to raise_error(Tinycode::ParseError, 'Unexpected end of stream')
      end

      example 'Malformed boolean values' do
        expect { described_class.load('?') }.to raise_error(Tinycode::ParseError, 'Malformed boolean value')
        expect { described_class.load('??') }.to raise_error(Tinycode::ParseError, 'Malformed boolean value')
        expect { described_class.load('?t') }.to raise_error(Tinycode::ParseError, 'Malformed boolean value')
        expect { described_class.load('?f') }.to raise_error(Tinycode::ParseError, 'Malformed boolean value')
        expect { described_class.load('?%') }.to raise_error(Tinycode::ParseError, 'Malformed boolean value')
      end
    end
  end
end
